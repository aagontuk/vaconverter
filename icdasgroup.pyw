#!/usr/bin/env python

import sys
from os import path
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
import groups

class ICDASGroup(QMainWindow):
    def __init__(self):
        super().__init__()

        layout = QGridLayout()

        self.btnInput = QPushButton("Browse")
        self.btnOutput = QPushButton("Browse")
        self.btnConvert = QPushButton("Convert")

        self.btnInput.clicked.connect(self.openInputDialog)
        self.btnOutput.clicked.connect(self.openOutputDialog)
        self.btnConvert.clicked.connect(self.convert)

        self.lblInput = QLabel("Input File:")
        self.lblOutput = QLabel("Output File Name:")
        self.lblSheet = QLabel("Sheet Name:")
        self.lblRowStart = QLabel("Start Row Number:")
        self.lblRowEnd = QLabel("End Row Number:")
        
        self.editInput = QLineEdit()
        self.editOutput = QLineEdit()
        self.editSheet = QLineEdit()
        self.spinRowStart = QSpinBox()
        self.spinRowStart.setRange(0, 1000000)
        self.spinRowEnd = QSpinBox()
        self.spinRowEnd.setRange(0, 1000000)

        layout.addWidget(self.lblInput, 0, 0, 1, 1)
        layout.addWidget(self.editInput, 1, 0, 1, 4)
        layout.addWidget(self.btnInput, 1, 4, 1, 1)
        layout.addWidget(self.lblSheet, 2, 0, 1, 1)
        layout.addWidget(self.editSheet, 2, 1, 1, 3)
        layout.addWidget(self.lblRowStart, 3, 0, 1, 1)
        layout.addWidget(self.spinRowStart, 3, 1, 1, 1)
        layout.addWidget(self.lblRowEnd, 3, 2, 1, 1)
        layout.addWidget(self.spinRowEnd, 3, 3, 1, 1)
        layout.addWidget(self.lblOutput, 4, 0, 1, 1)
        layout.addWidget(self.editOutput, 5, 0, 1, 4)
        layout.addWidget(self.btnOutput, 5, 4, 1, 1)
        layout.addWidget(self.btnConvert, 6, 4, 1, 1)

        widget = QWidget()
        widget.setLayout(layout)

        self.setGeometry(500, 200, 400, 100)
        self.setCentralWidget(widget)
        self.setWindowTitle("VA Data Converter")
        self.show()

    def openInputDialog(self):
        openIn = path.expanduser("~") + "//Documents" 
        filename, _ = QFileDialog.getOpenFileName(self, "Input File", openIn, "Excel 97-2003 Workbook (*.xls);; Excel Workbook (*.xlsx)")
        self.editInput.setText(filename)

    def openOutputDialog(self):
        openIn = path.expanduser("~") + "//Documents" 
        filename, _ = QFileDialog.getOpenFileName(self, "Output File", openIn, "Excel 97-2003 Workbook (*.xls);; Excel Workbook (*.xlsx)")
        self.editOutput.setText(filename)

    def convert(self):
        inputFile = self.editInput.text()
        outputFile = self.editOutput.text()
        sheetName = self.editSheet.text()
        startRow = self.spinRowStart.value()
        endRow = self.spinRowEnd.value()
        groups.convert(inputFile, outputFile, sheetName, startRow, endRow)
        self.statusBar().showMessage("Successfully converted!", 5000)

if __name__ == "__main__":
    app = QApplication(sys.argv)
    gui = ICDASGroup()
    app.exec_()
