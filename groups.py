#!/usr/bin/env python

from os import path
import xlwings as xw
import pandas as pd

def convert(inputFile, outputFile, sheetName, startRow, endRow):
	if not path.isfile(outputFile):
		outputFile = path.dirname(inputFile) + "\\" + outputFile 

	wb = xw.Book(inputFile)
	sht = wb.sheets[sheetName]
	
	dis_range_str = "A" + str(startRow) + ":" + "A" + str(endRow)
	sex_range_str = "B" + str(startRow) + ":" + "B" + str(endRow)
	age_range_str = "C" + str(startRow) + ":" + "C" + str(endRow)
	
	diseases = sht.range(dis_range_str).value
		
	sex = sht.range(sex_range_str).value
	sex = list(map(int, sex))
		
	rages = sht.range(age_range_str).options(numbers=int).value
	ages = []
	
	for age in rages:
		if age:
			if age.endswith(' month') or age.endswith(' months') or age.endswith(' day') or age.endswith(' days'):
				ages.append(0)
			elif age.endswith(' year'):
				ages.append(1)
			else:
				ages.append(age.strip(' years'))

	ages = list(map(int, ages))

	uniqdiseases = set(diseases)

	agegroups = sht.range('D1:Y1').value

	groups = {}
	for age in agegroups:
		groups[age] = {}

	for dis in uniqdiseases:
		for key in groups.keys():
			groups[key][dis] = []

	for dis in uniqdiseases:
		for key in groups.keys():
			for i in range(2):
				groups[key][dis].append(0)

	for dis, age, s in zip(diseases, ages, sex):
		if age < 1:
			if s == 1:
				groups[agegroups[0]][dis][0] += 1
			else:
				groups[agegroups[0]][dis][1] += 1
		elif age >= 1 and age <= 4:
			if s == 1:
				groups[agegroups[1]][dis][0] += 1
			else:
				groups[agegroups[1]][dis][1] += 1
		elif age >= 5 and age <= 9:
			if s == 1:
				groups[agegroups[2]][dis][0] += 1
			else:
				groups[agegroups[2]][dis][1] += 1
		elif age >= 10 and age <= 14:
			if s == 1:
				groups[agegroups[3]][dis][0] += 1
			else:
				groups[agegroups[3]][dis][1] += 1
		elif age >= 15 and age <= 19:
			if s == 1:
				groups[agegroups[4]][dis][0] += 1
			else:
				groups[agegroups[4]][dis][1] += 1
		elif age >= 20 and age <= 24:
			if s == 1:
				groups[agegroups[5]][dis][0] += 1
			else:
				groups[agegroups[5]][dis][1] += 1
		elif age >= 25 and age <= 29:
			if s == 1:
				groups[agegroups[6]][dis][0] += 1
			else:
				groups[agegroups[6]][dis][1] += 1
		elif age >= 30 and age <= 34:
			if s == 1:
				groups[agegroups[7]][dis][0] += 1
			else:
				groups[agegroups[7]][dis][1] += 1
		elif age >= 35 and age <= 39:
			if s == 1:
				groups[agegroups[8]][dis][0] += 1
			else:
				groups[agegroups[8]][dis][1] += 1
		elif age >= 40 and age <= 44:
			if s == 1:
				groups[agegroups[9]][dis][0] += 1
			else:
				groups[agegroups[9]][dis][1] += 1
		elif age >= 45 and age <= 49:
			if s == 1:
				groups[agegroups[10]][dis][0] += 1
			else:
				groups[agegroups[10]][dis][1] += 1
		elif age >= 50 and age <= 54:
			if s == 1:
				groups[agegroups[11]][dis][0] += 1
			else:
				groups[agegroups[11]][dis][1] += 1
		elif age >= 55 and age <= 59:
			if s == 1:
				groups[agegroups[12]][dis][0] += 1
			else:
				groups[agegroups[12]][dis][1] += 1
		elif age >= 60 and age <= 64:
			if s == 1:
				groups[agegroups[13]][dis][0] += 1
			else:
				groups[agegroups[13]][dis][1] += 1
		elif age >= 65 and age <= 69:
			if s == 1:
				groups[agegroups[14]][dis][0] += 1
			else:
				groups[agegroups[14]][dis][1] += 1
		elif age >= 70 and age <= 74:
			if s == 1:
				groups[agegroups[15]][dis][0] += 1
			else:
				groups[agegroups[15]][dis][1] += 1
		elif age >= 75 and age <= 79:
			if s == 1:
				groups[agegroups[16]][dis][0] += 1
			else:
				groups[agegroups[16]][dis][1] += 1
		elif age >= 80 and age <= 84:
			if s == 1:
				groups[agegroups[17]][dis][0] += 1
			else:
				groups[agegroups[17]][dis][1] += 1
		elif age >= 85 and age <= 89:
			if s == 1:
				groups[agegroups[18]][dis][0] += 1
			else:
				groups[agegroups[18]][dis][1] += 1
		elif age >= 90 and age <= 94:
			if s == 1:
				groups[agegroups[19]][dis][0] += 1
			else:
				groups[agegroups[19]][dis][1] += 1
		elif age >= 95:
			if s == 1:
				groups[agegroups[20]][dis][0] += 1
			else:
				groups[agegroups[20]][dis][1] += 1

	pddict = {}
	pddict['sex'] = []

	disindex = []
	for dis in uniqdiseases:
		for i in range(2):
			disindex.append(dis)
			pddict['sex'].append(i+1)

	for age in agegroups:
		pddict[age] = []

	for age in agegroups:
		for dis in uniqdiseases:
			for i in range(2):
				pddict[age].append(groups[age][dis][i])

	df = pd.DataFrame(pddict, index = disindex)
	writer = pd.ExcelWriter(outputFile)
	df.to_excel(writer)
	writer.save()
